# eselect-electron-bin

# [eselect](https://wiki.gentoo.org/wiki/Project:Eselect) module for [Electron](http://electron.atom.io/)

**eselect-electron** is a eselect module allowing to switch the default Electron-bin slot.

